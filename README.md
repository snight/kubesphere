# KubeSphere

## kubekey

kubeykey是KubeSphere基于Go语言开发的kubernetes集群安装工具，可以轻松、高效、灵活地单独或整体安装 Kubernetes 和 KubeSphere，底层使用 Kubeadm 在多个节点上并行安装 Kubernetes 集群，支持创建、扩缩和升级 Kubernetes 集群。
KubeKey 提供内置高可用模式，支持一键安装高可用 Kubernetes 集群，提供了All-in-One模式，多节点安装，离线安装等多种方式。

## ks-install

在已有 Kubernetes 集群部署 KubeSphere 的工具
